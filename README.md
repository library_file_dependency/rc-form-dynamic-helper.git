### rc-form-dynamic-helper

搭配[rc-form](https://www.npmjs.com/package/rc-form)体系使用的，表单动态项助手



> 功能特色

* 完美搭配[rc-form](https://www.npmjs.com/package/rc-form)和antd的form组件
* 支持动态排序

> api简介

| api           | 描述                                    |
| ------------- | --------------------------------------- |
| DynamicFields | 动态项组件                              |
| withDynamic   | 高阶组件，用于获取DynamicFields的上下文 |

> 快捷api

| api          | 描述                         |
| ------------ | ---------------------------- |
| InsertButton | 插入动作的快捷封装           |
| RemoveButton | 移除动作的快捷封装           |
| Move2Prev    | 将当前项与上一项进行交换操作 |
| Move2Next    | 将当前项与下一项进行交换操作 |



> DynamicFields

| props        | type           | defaultValue      |
| ------------ | -------------- | ----------------- |
| form         | fromShape      | rc-fofm fromShape |
| name         | 动态项的字段名 | “”                |
| initialValue | array[]        | []                |

> 使用案例
>

![demo](https://gitee.com/library_file_dependency/rc-form-dynamic-helper/raw/master/assets/demo.gif)

```react
// TestPage.js
import React from "react";
import { Form } from "antd";
import DynamicItem from "@/components/DynamicItem";
import { InsertButton, DynamicFields } from "../../src";


@Form.create({
  name: "TestPage",
  onValuesChange(props, changValues, allValues) {
    console.log(allValues);
  }
})
class TestPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { initialValue: undefined };
  };

  handleResetAll = () => {
    const { form } = this.props;
    form.resetFields();
  };

  render() {
    const { form } = this.props;
    const { initialValue } = this.state;
    return (
      <div style={{ width: 500, margin: "0px auto" }}>
        <DynamicFields name="test" form={form} initialValue={initialValue}>
          {($list) => {
            return (
              <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
                <InsertButton dataShape={{ name: `name(${$list.length})`, age: `age(${$list.length})` }} />
                <button onClick={this.handleResetAll}>重置所有</button>
                {$list.map((initialValue, index) => (
                  <DynamicItem key={index} index={index} form={form} initialValue={initialValue} />
                ))}
              </Form>)
          }}
        </DynamicFields>
      </div>)
  };
};
export default TestPage;

```

```react
// DynamicItem.js(每一项)
import React from "react";
import { Row, Col, Form, Input } from "antd";
import { RemoveButton, Move2Prev, Move2Next } from "../../src";

class DynamicItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  };

  render() {
    const { form: { getFieldDecorator }, index, initialValue } = this.props;
    return (
      <Row type="flex" gutter={10} align="middle" style={{ margin: "10px 0px" }}>
        <Col span={8}>
          <Form.Item style={{ margin: 0 }} label="name">
            {getFieldDecorator(`test[${index}].name`, {
              initialValue: initialValue.name,
              rules: [{ required: true }]
            })(
              <Input style={{ width: "100%" }} allowClear />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item style={{ margin: 0 }} label="age">
            {getFieldDecorator(`test[${index}].age`, {
              initialValue: initialValue.age,
              rules: [{ required: true }]
            })(
              <Input style={{ width: "100%" }} allowClear />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Move2Prev index={index} />
          <Move2Next index={index} />
          <RemoveButton index={index} />
        </Col>
      </Row>)
  };
};

export default DynamicItem;
```



