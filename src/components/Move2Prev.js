import React from "react";
import withDynamic from "../services/withDynamic";


@withDynamic
class Move2Prev extends React.Component {

  handleMove2Prev = () => {
    const { $dynamic, index } = this.props;
    $dynamic.move2prev(index);
  };

  get computed() {
    const { index } = this.props;
    if (index === 0) {
      return { disabled: true, style: { color: "#ffffff", background: "#888888" } };
    };
  };

  render() {
    return (
      <button {...this.computed} onClick={this.handleMove2Prev}>
        上移
      </button>)
  };

};
export default Move2Prev;