import React from "react";
import withDynamic from "../services/withDynamic";


@withDynamic
class Move2Next extends React.Component {

  handleMove2Next = () => {
    const { $dynamic, index } = this.props;
    $dynamic.move2next(index);
  };

  get computed() {
    const { $dynamic: { $list }, index } = this.props;
    if (index === ($list.length - 1)) {
      return { disabled: true, style: { color: "#ffffff", background: "#888888" } };
    };
  };

  render() {
    return (
      <button {...this.computed} onClick={this.handleMove2Next}>
        下移
      </button>)
  };

};
export default Move2Next;