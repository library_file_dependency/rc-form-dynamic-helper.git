import React from "react";
import withDynamic from "../services/withDynamic";

@withDynamic
class RemoveButton extends React.Component {

  handleRemove = () => {
    const { $dynamic, index } = this.props;
    $dynamic.remove(index);
  };

  render() {
    return (
      <button onClick={this.handleRemove}>
        删除
      </button>)
  };
};

export default RemoveButton;