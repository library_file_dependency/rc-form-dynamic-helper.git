import React from "react";
import withDynamic from "../services/withDynamic";

@withDynamic
class InsertButton extends React.Component {

  static defaultProps = {
    dataShape: {}
  };

  handleInsert = () => {
    const { $dynamic, dataShape } = this.props;
    $dynamic.insert(dataShape);
  };

  render() {
    return (
      <button onClick={this.handleInsert}>
        新增动态项
      </button>)
  };
};

export default InsertButton;