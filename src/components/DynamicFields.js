import React from "react";
import propTypes from "prop-types";
import { formShape } from "rc-form";
import DynamicContext from "../utils/DynamicContext";


class DynamicFields extends React.Component {

  static propsTypes = {
    form: formShape,
    name: propTypes.string.isRequired,
    initalValue: propTypes.array
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.initialValue) { return null };
    if (nextProps.initialValue) {
      return {
        $list: prevState.$list.concat(nextProps.initialValue),
        initialValue: nextProps.initialValue
      };
    };
    return null;
  };

  constructor(props) {
    super(props);
    this.state = { $list: [], initialValue: null };
  };

  handleMove2Prev = async (index) => {
    // 将当前动态项和上一项交换
    const { form, name } = this.props;
    const formSnapShot = form.getFieldValue(name);
    const prevItem = formSnapShot[index - 1];
    const currentItem = formSnapShot[index];
    await this.setState({ $list: [] });
    await form.setFieldsValue({ [name]: [] });
    formSnapShot[index - 1] = currentItem;
    formSnapShot[index] = prevItem;
    await this.setState({ $list: formSnapShot });
    await form.setFieldsValue({ [name]: formSnapShot });
  };

  handleMove2Next = async (index) => {
    // 将当前动态项和下一项交换
    const { form, name } = this.props;
    const formSnapShot = form.getFieldValue(name);
    const currentItem = formSnapShot[index];
    const nextItem = formSnapShot[index + 1];
    await this.setState({ $list: [] });
    await form.setFieldsValue({ [name]: [] });
    formSnapShot[index + 1] = currentItem;
    formSnapShot[index] = nextItem;
    await this.setState({ $list: formSnapShot });
    await form.setFieldsValue({ [name]: formSnapShot });
  };

  handleInsert = async (valueShape = {}) => {
    // 新增动态项
    const { $list } = this.state;
    const { form, name } = this.props;
    const formSnapShot = (form.getFieldValue(name) || []);
    if (valueShape instanceof Array) {
      const newList = valueShape.concat(formSnapShot);
      await this.setState({ $list: newList });
      await form.setFieldsValue({ [name]: newList });
    } else {
      await this.setState({ $list: [] });
      await form.setFieldsValue({ [name]: [] });
      formSnapShot.unshift(valueShape);
      await this.setState({ $list: formSnapShot });
      await form.setFieldsValue({ [name]: formSnapShot });
    };
  };

  handleRemove = async (index) => {
    // 移除当前动态项
    const { form, name } = this.props;
    const formSnapShot = form.getFieldValue(name);
    formSnapShot.splice(index, 1);
    await this.setState({ $list: [] });
    await form.setFieldsValue({ [name]: [] });
    await this.setState({ $list: formSnapShot })
    await form.setFieldsValue({ [name]: formSnapShot })
  };

  render() {
    const { $list } = this.state;
    const { children } = this.props;
    return (
      <DynamicContext.Provider
        value={{
          $list,
          insert: this.handleInsert,
          remove: this.handleRemove,
          move2prev: this.handleMove2Prev,
          move2next: this.handleMove2Next
        }}
      >
        {children($list)}
      </DynamicContext.Provider>)
  };
};

export default DynamicFields;

/*
  <DynamicFields name="group">
    {($list)=>(
      <React.Fragment>
        <InsertButton/>
        {$list.map(()=>{

        })}
      </React.Fragment>
    )}
  </DynamicFields>
*/
