import React from "react";
import hoistNonReactStatics from "hoist-non-react-statics";

import DynamicContext from "../utils/DynamicContext";


export default function withDynamic(SourceComponent) {
  function TargetComponent(props, ref) {
    return (
      <DynamicContext.Consumer>
        {(context) => (<SourceComponent {...props} $dynamic={context} ref={ref} />)}
      </DynamicContext.Consumer>)
  };
  hoistNonReactStatics(SourceComponent, TargetComponent);
  return React.forwardRef(TargetComponent);
};