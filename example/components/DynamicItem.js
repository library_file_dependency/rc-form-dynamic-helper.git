import React from "react";
import { Row, Col, Form, Input } from "antd";
import { RemoveButton, Move2Prev, Move2Next } from "../../src";

class DynamicItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  };

  render() {
    const { form: { getFieldDecorator }, index, initialValue } = this.props;
    return (
      <Row type="flex" gutter={10} align="middle" style={{ margin: "10px 0px" }}>
        <Col span={8}>
          <Form.Item style={{ margin: 0 }} label="name">
            {getFieldDecorator(`test[${index}].name`, {
              initialValue: initialValue.name,
              rules: [{ required: true }]
            })(
              <Input style={{ width: "100%" }} allowClear />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item style={{ margin: 0 }} label="age">
            {getFieldDecorator(`test[${index}].age`, {
              initialValue: initialValue.age,
              rules: [{ required: true }]
            })(
              <Input style={{ width: "100%" }} allowClear />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Move2Prev index={index} />
          <Move2Next index={index} />
          <RemoveButton index={index} />
        </Col>
      </Row>)
  };
};

export default DynamicItem;