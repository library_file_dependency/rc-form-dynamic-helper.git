import React from "react";
import ReactDOM from "react-dom";
import TestPage from "@/pages/TestPage";


(function render() {
  const mountNode = document.createElement("div");
  document.body.appendChild(mountNode);
  ReactDOM.render((
    <div style={{ position: "absolute", left: "50%", top: 100, transform: "translateX(-50%)" }}>
      <TestPage />
    </div>
  ), mountNode);
})();
