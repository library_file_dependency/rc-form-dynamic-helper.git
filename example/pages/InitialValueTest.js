import React from "react";
import { Form, Input } from "antd";

// 探究initialValue

@Form.create({ name: "initialValue" })
class InitialValueTest extends React.Component {

  constructor(props) {
    super(props);
    this.state = { value: undefined };
  };

  async componentDidMount() {
    const { form: { setFieldsValue } } = this.props;
    await new Promise((resolve) => {
      setTimeout(resolve, 2000);
    });
    this.setState({ value: "test1" });
    await new Promise((resolve) => {
      setTimeout(resolve, 2000);
    });
    setFieldsValue({ test: "4454" });
    await new Promise((resolve) => {
      setTimeout(resolve, 2000);
    });
    this.setState({ value: "test2" });
  };

  render() {
    const { value } = this.state;
    const { form: { getFieldDecorator } } = this.props;
    return (
      <Form style={{ width: 900, margin: "0px auto" }}>
        <Form.Item>
          {getFieldDecorator("test", {
            initialValue: value
          })(
            <Input allowClear />
          )}
        </Form.Item>
      </Form>)
  };
};
export default InitialValueTest;