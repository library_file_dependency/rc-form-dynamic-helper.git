import React from "react";
import { Form } from "antd";
import DynamicItem from "@/components/DynamicItem";
import { InsertButton, DynamicFields } from "../../src";


@Form.create({
  name: "TestPage",
  onValuesChange(props, changValues, allValues) {
    console.log(allValues);
  }
})
class TestPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { initialValue: undefined };
  };

  handleResetAll = () => {
    const { form } = this.props;
    form.resetFields();
  };

  render() {
    const { form } = this.props;
    const { initialValue } = this.state;
    return (
      <div style={{ width: 500, margin: "0px auto" }}>
        <DynamicFields name="test" form={form} initialValue={initialValue}>
          {($list) => {
            return (
              <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
                <InsertButton dataShape={{ name: `name(${$list.length})`, age: `age(${$list.length})` }} />
                <button onClick={this.handleResetAll}>重置所有</button>
                {$list.map((initialValue, index) => (
                  <DynamicItem key={index} index={index} form={form} initialValue={initialValue} />
                ))}
              </Form>)
          }}
        </DynamicFields>
      </div>)
  };
};
export default TestPage;